# Food_waste_app
    Specificații detaliate, planul de proiect:  
	    Realizarea unei aplicații web pentru a evita risipa consumului de alimente.  
	        	    Rolurile fiecărui membru:  
		Creare repository:Mogoș Andreea 
		Planificare logică aplicație: Mogos, Olteanu, Munteanu, Paraschiv, Negru
	    Front-end: Olteanu Laura, Mogoș Andreea,  Negru Ana  
				-React.js, HTML, CSS, JavaScript, PhpMyAdmin
				-Structura aplicației:    
					-Bara de navigare
					-Dupa Log In/Register, pe profile sunt datele despre "frigider"
					-Pagină de profile design: Olteanu Laura și Negru Ana
	    Back-end:Mogoș Andreea-Maria, Munteanu Alina
				-Mogoș Andreea: creare pagină de login și signup și back-end pentru aceste operații.
							  : design bara de navigatie și home
							  :creare baza de date pentru produse si operatii pentru aceasta
							  :afisarea produselor in lista si filtrarea dupa categorie
							  :crearea bazei de date pentru prieteni si afisarea in containere  
				-Munteanu: filtrare produse dupa categorie si alerta la expirare 
				-Paraschiv:integrare cu instagram și facebook  
		Baza de date a fost creată în XAMPP cu PHPMyAdmin și exportată.
				