import React from 'react';
import{ BrowserRouter as Router, Route} from 'react-router-dom'
import NavigationBar from './Components/NavigationBar'
import HomeComponent from './Components/HomeComponent'
import LoginComponent from './Components/LoginComponent'
import ProfileComponent from './Components/ProfileComponent'
import RegisterComponent from './Components/RegisterComponent'
import ListComponent from './Components/ListComponent'
import FriendsComponent from './Components/FriendsComponent'

function App() {
  return (
    <Router>
      <div className="App">
        <NavigationBar/>  
        <Route exact path="/" component={HomeComponent}/>
        <div className="container">
          <Route exact path="/register" component={RegisterComponent}/>
          <Route exact path="/login" component={LoginComponent}/>
          <Route exact path="/profile" component={ProfileComponent}/>
          <Route exact path="/list" component={ListComponent}/>
          <Route exact path="/friends" component={FriendsComponent}/>
        </div>
      </div>
    </Router>
  );
}

export default App;
