import React, { Component } from 'react'
import { register } from './UsersFunc'
import './Register.css'

class RegisterComponent extends Component {
  constructor() {
    super()
    this.state = {
      nume:'',
      email: '',
      password: ''
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange(e) {
    //pentru actualizarea input-urilor cu datele introduse
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit(e) {
    e.preventDefault()

    const newUser = {
     nume:this.state.nume,
      email: this.state.email,
      password: this.state.password
    }

    register(newUser).then(res => {
      this.props.history.push(`/login`)
    })
  }

  render() {
    return (
      <div className="backk-img">
      <div className="container">
        <div className="row">
          <div className="col-md-6 mt-5 mx-auto">
            <form noValidate onSubmit={this.onSubmit}>
              <h1 className="register-text">Register</h1>
              <div className="form-group">
                <label htmlFor="name">Nume</label>
                <input
                  type="text"
                  className="form-control"
                  name="nume"
                  placeholder="Type your name"
                  value={this.state.nume}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email address</label>
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  placeholder="Enter email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
              </div>
              <button
                type="submit"
                className="btn-register"
              >
                Register!
              </button>
            </form>
          </div>
        </div>
      </div>
      </div>

    )
  }
}

export default RegisterComponent