import axios from 'axios' //clientul HTTP browser si nodejs

export const register = newUser => {
  //promisiune bazata de clientul HTTP
  return axios
    .post('users/register', {
      nume:newUser.nume,
      email: newUser.email,
      password: newUser.password
    })
    .then(response => {
      console.log('Registered')
    })
}

export const login = user => {
  return axios
    .post('users/login', {
      email: user.email,
      password: user.password
    })
    .then(response => {
      localStorage.setItem('usertoken', response.data)
      return response.data
    })
    .catch(err => {
      alert("Inncorect credentials or does not exist")
      console.log(err)
    })
}

export const addProduct = newProduct =>{
  if(!isNaN(newProduct.cantity)){
    return axios
    .post('/profile/products',{
      name:newProduct.name,
      category:newProduct.category,
      valability:newProduct.valability,
      cantity:parseFloat(newProduct.cantity),
      unit:newProduct.unit,
      note:newProduct.note
    })
  }
  else
  {
    window.alert("Cantity is not a number")
  }
}

export const showByCategory=product=>{
  try{
    return axios
    .get('/profile/products/:category',{
      params:{
        category:product.category
      }
    })
  }catch{
    console.warn("error")
  }

}
