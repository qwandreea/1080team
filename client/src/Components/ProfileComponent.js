import React, { Component } from 'react'
import jwt_decode from 'jwt-decode'
import './Profile.css'
import { addProduct } from './UsersFunc'
const value="2020-01-01"
const veg_bascket=require('../img/vegetables_basket_red.png')
const tomatoes=require('../img/tomatoes.jpg')
const milk=require('../img/milk.jpg')
const potatoes=require('../img/potatoes.jpg')


class ProfileComponent extends Component {
  constructor() {
    super()
    this.state = {
      nume:'',
      email: '',
      value:'',
      name:'',
      category:'',
      valability:'',
      cantity:'',
      unit:'',
      note:''
    }
    this.onChange = this.onChange.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
  }

  // fileSelected=event=>{
  //   this.setState({
  //     selected:event.target.files[0]
  //   })
  // }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  onSubmit(e){
    e.preventDefault()

    const product={
      name: this.state.name,
      category: this.state.category,
      valability: this.state.valability,
      cantity:parseFloat(this.state.cantity),
      unit:this.state.unit,
      note: this.state.note
    }

    addProduct(product).then(res=>{
      if(res){
       window.alert("Your product named " +product.name+" was added ")
      }
    })
  }

  componentDidMount() {
    const token = localStorage.usertoken
    const decoded = jwt_decode(token)
    this.setState({
      nume:decoded.nume,
      email: decoded.email
    })
  }

  render() {
    return (
      <body>
         <header>
            <nav>
              <div className="row">
                  <ul className="main-nav">
                  <li> <a href="/list">My List</a></li>
                  <li> <a href="/friends">My Friends</a></li>
                  </ul>
                  </div>
              </nav>
		
               <div className="hero-text-box">
               <h1>Goodbye food waste. <br></br> Hello sustainability.</h1>
               <a className="btn btn-full" href="profile/#form" >Add My List</a>
               <a className="btn btn-ghost js--scroll-to-list" href="/list">Show My List </a>
             </div>
      </header>
      <section className="section-steps" id="works">
        <div className="row">
          <h2>
          How it works &mdash; Simple as 1, 2, 3
          </h2>
          
       </div>
      <div className="row">
          <div className="col span-1-of-2 steps-box">
              <img src={veg_bascket} alt="Vegetable Basket" className="veg_bascket"/>
          </div>
          <div className="col span-1-of-2 steps-box">
              <div className="works-step">
              <div>1</div>
                  <p>Take pics of the food from your fridge and add them to your list.</p>
              </div>
              
              <div className="works-step">
              <div>2</div>
                  <p>Share with your friends the products soon to expire or the ones you don't want anymore.</p>
              </div>
              
              <div className="works-step">
              <div>3</div>
                  <p>Share your fridge to your Facebook and Instagram friends and let them know how to live a sustainable life. </p>
              </div>         
           </div>   
          </div>
      </section>

      <section className="form js--section-form">
      <a  id="form"></a>
        <div className="row">
            <h2> Share & Sustain</h2>
        </div>

        <div className="row">
        <form  className="contact-form" noValidate onSubmit={this.onSubmit}> 
        <div className="row">
        <div className="col span-1-of-3">
                <label htmlFor="name"> Product name </label>
          </div>
         
          <div className="col span-2-of-3">
            <input type="text" name="name" id="name" onChange={this.onChange} 
            value={this.state.name}
            placeholder="Product name" required/>  
          </div>    
        </div>

        <div className="row">
          <div className="col span-2-of-3">
            <label htmlFor="category">Category</label>
          </div>
          <div className="col span-2-of-3">
            <form>
              <select name="category" id="category" value={this.state.category} onChange={this.onChange} >
              	<option value="Vegetables">Vegetables</option>
                <option value="Fruits">Fruits</option>
                <option value="Dairy Products">Dairy Products</option>
                <option value="Meat">Meat</option>
                <option value="Natural juice">Natural juice</option>
                <option value="Sweets">Sweets</option>
                <option value="Others">Others</option>
             </select>
           </form>
          </div>    
        </div>


          <div className="row">
            <div className="col span-1-of-3">
                <label htmlFor="valability">  Valability </label>
            </div>
            <div className="col span-2-of-3">
                <input type="date" name="valability" 
                value={this.state.valability}
                onChange={this.onChange}
                id="valability"  min="2020-01-01" max="2025-12-31"  placeholder="valability" required/>  
            </div>   
            </div>


            <div className="row">
            <div className="col span-1-of-3">
                <label htmlFor="cantity">  Cantity </label>
            </div>
            <div className="col span-2-of-3">
                <input type="text" name="cantity" 
                value={this.state.cantity}
                onChange={this.onChange}
                id="cantity"   placeholder="cantity" required/>  
            </div>   
            </div>

            <div className="row">
            <div className="col span-1-of-3">
                <label htmlFor="unit">  Unit </label>
            </div>
            <div className="col span-2-of-3">
            <form>
              <select name="unit" id="unit" value={this.state.unit} onChange={this.onChange} >
                <option value="Grams">Grams</option>
                <option value="Kg">Kg</option>
                <option value="Liters">Liters</option>
                <option value="Pieces">Pieces</option>
                <option value="Teaspoon">Teaspoon</option>
                <option value="Cup"></option>
             </select>
            </form>
           </div>   
           </div>

            <div className="row">
              <div className="col span-2-of-3">
                <label htmlFor="note"> Note for friends </label>
              </div>
            <div className="col span-2-of-3">
              <textarea name="note" id="note" 
              value={this.state.note}
              onChange={this.onChange} placeholder="Your note" required/> 
            </div>  
            </div>

            <div className="row">
                <div className="col span-1-of-3">
                    <label> &nbsp; </label>
                 </div>
                 <div className="col span-2-of-3">
                    <input type="submit" className="add_button"  onChange={this.onChange} value="Add it!"/>
                  </div>    
            </div>

            </form>
          </div>
        </section>

        {/* <section className="section-list js--section-list">
            <a  id="my-list"></a>
            <div className="row">
            <h2>
                My List
                </h2>
            </div>
            <div className="row">
            <div className="col span-1-of-3">
            <div className="product-box">
            <div>
                <img src={tomatoes}  alt="tomatoes"/>
            </div>
            <div>
               <ul>
                <li><i className="ion-ios-trash:before icon-small"></i>PRODUCT NAME</li>
                <li><i className="ion-ios-trash:before icon-small"></i>CATEGORY</li>
                <li><i className="ion-ios-checkmark icon-small"></i>VALABILITY</li>
                <li><i className="ion-ios-checkmark icon-small"></i>NOTES</li>   
                <li><i className="ion-ios-checkmark icon-small"></i>OWNER</li>            
               </ul>
            </div>
            <div>
            <a href="#" className="btn btn-full">Claim</a>
            <a href="#" className="btn btn-full">Delete</a>
            </div>
                
            </div>    
                
                
            </div>
                
                 <div className="col span-1-of-3">
            <div className="product-box">
            <div>
                <img src={milk}  alt="milk"/>
            </div>
            <div>
               <ul>
                <li><i className="ion-ios-checkmark icon-small"></i>1 meal 10 days/month</li>
                <li><i className="ion-ios-checkmark icon-small"></i>Order 24/7</li>
                <li><i className="ion-ios-checkmark icon-small"></i>Access to newest creations</li>
                <li><i className="ion-ios-checkmark icon-small"></i>Free delivery</li>
                
                
                </ul>
            </div>
            <div>
            <a href="#" className="btn btn-ghost">Sign up now</a>
            </div>
                
            </div>    
                
                
            </div>
            
             <div className="col span-1-of-3">
            <div className="product-box">
            <div>
                    <img src={potatoes} alt="potatoes"/>
            </div>
            <div>
               <ul>
                <li><i className="ion-ios-checkmark icon-small"></i>1 meal</li>
                <li><i className="ion-ios-checkmark icon-small"></i>Order from 8 am to 12 pm</li>
                <li><i className="ion-ios-close icon-small"></i></li>
                <li><i className="ion-ios-checkmark icon-small"></i>Free delivery</li>
                </ul>
            </div>
            <div>
            <a href="#" className="btn btn-ghost">Sign up now</a>
            </div>
            </div>    
            </div>
            </div>
        
        </section> */}
      </body>
    )
  }
  
}

export default ProfileComponent