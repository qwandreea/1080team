import React, { Component } from 'react'
import './List.css'
import $ from 'jquery'
import axios from 'axios'
import {showByCategory} from './UsersFunc'
import MetaTags from 'react-meta-tags';
import 'bootstrap/dist/css/bootstrap.css';
import Iframe from 'react-iframe'

const veg_bascket=require('../img/vegetables_basket_red.png')
const nutrition=require('../img/nutrition.png')

class ListComponent extends Component{
    constructor(){
        super()
        this.state={
          name:'',
          category:'',
          valability:'',
          cantity:'',
          unit:'',
          note:''
        }
        this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
      this.setState({ [e.target.name]: e.target.value })
    }


    componentDidMount() {
        function fetchProducts(done){
            $.get('/profile/products', function(data){
              done(data)
            })
          }


          $(function(e){
            let productList=$("#product-box")
           
           fetchProducts(function(products){

              $("#category").change(function () {
                var display=$("#category option:selected").val();
                
                productList.empty()
                for(var product of products){
                    if(product.category==display)
                    {
                      productList.append(createProductCard(product))
                      var today = new Date()
                      var expireDate = product.valability
                      var timeUntilExpireDate =  Date.parse(expireDate)- today.getTime()
                      var daysUntilExpireDate = Math.floor(timeUntilExpireDate / (1000*3600*24))
                      if(daysUntilExpireDate < 3){
                          alert("You have  " + daysUntilExpireDate + " day until the product " + product.name + " expires!")
                       }
                    }
                    else if(display=='All')
                   {
                      productList.append(createProductCard(product))
                      var today = new Date()
                      var expireDate = product.valability
                      var timeUntilExpireDate =  Date.parse(expireDate)- today.getTime()
                      var daysUntilExpireDate = Math.floor(timeUntilExpireDate / (1000*3600*24))
                      if(daysUntilExpireDate < 3){
                          alert("You have  " + daysUntilExpireDate + " day until the product " + product.name + " expires!")
                       }
                    }
                  }
              })
            })
          })

          function createProductCard(product){
            return $(`
            <div class="row">
            <div className="col span-1-of-3" id="col span">
            <div className="product-box" id="product-box">
             <div>
                 <img src=${nutrition}  alt="food"/>
             </div>
        <div className="col-4 card mx-2 p-2">
           <ul>
            <li><i className="ion-ios-trash:before icon-small"></i>PRODUCT NAME:   <span class="span">${product.name}</span> </li>
            <li><i className="ion-ios-trash:before icon-small"></i>PRODUCT CATEGORY:     <span class="span">${product.category}</span></li>
            <li><i className="ion-ios-checkmark icon-small"></i>VALABILITY TILL:    <span class="span"> ${product.valability}</span></li>
            <li><i className="ion-ios-checkmark icon-small"></i>CANTITY:    <span class="span">${product.cantity}</span></li>   
            <li><i className="ion-ios-checkmark icon-small"></i>UNIT:     <span class="span">${product.unit}</span></li>            
            <li><i className="ion-ios-checkmark icon-small"></i>NOTE FOR FRIENDS:     <span class="span">${product.note}</span></li>   
           </ul>
        </div>
        <div>
        <button class="btn btn-full" id="btn" >Claim</button>
        </div>
            </div>  
            </div>
            </div>
             
           ` )
          }
          
  
      }

    render(){
        return(
          <div>
          <div className="wrapper">
          <MetaTags>
            <title>Food Waste App</title>
            <meta property="og:url" content="http://82.137.31.194:3000/list" />
  	        <meta property="og:type"  content="website" />
 	          <meta property="og:title" content="Food Waste App" />
  	        <meta property="og:description" content="Come see my items" />
          </MetaTags>
        </div>
            <section className="section-list js--section-list">
            <div className="row">
          <h2>
          My List
          </h2>
          <div className="col span-2-of-3">
            <form>
              <select name="category" id="category" value={this.state.category}  onChange={this.onChange}   >
                <option value="" disabled selected>Select a category...</option>
                <option value="All">All</option>
                <option value="Dairy Products">Dairy Products</option>
                <option value="Fruits">Fruits</option>
              	<option value="Vegetables">Vegetables</option>
                <option value="Meat">Meat</option>
                <option value="Natural juice">Natural juice</option>
                <option value="Sweets">Sweets</option>
                <option value="Others">Others</option>
             </select>
           </form>
          </div>   
      </div>
      <div className="row">
      <div className="col span-1-of-3" id="col span">
      <div className="product-box" id="product-box">
      <div>
          <img src={nutrition}  alt="food"/>
      </div>
      <div className="col-4 card mx-2 p-2">
         <ul>
          <li><i className="ion-ios-trash:before icon-small"></i>PRODUCT NAME </li>
          <li><i className="ion-ios-trash:before icon-small"></i>CATEGORY  </li>
          <li><i className="ion-ios-checkmark icon-small"></i>VALABILITY  </li>
          <li><i className="ion-ios-checkmark icon-small"></i>NOTES  </li>   
          <li><i className="ion-ios-checkmark icon-small"></i>OWNER  </li>            
         </ul>
      </div>

      </div>  
       </div>
       </div>
        <div id="fb-root"></div>
        <script async defer crossOrigin="anonymous" src="https://connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v5.0&appId=448032569246936&autoLogAppEvents=1"></script>
        <div className="btn-facebook" data-href="http://82.137.31.194:3000/list" data-layout="button_count" data-size="large">
        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F82.137.31.194%3A3000%2Fprofile&amp;src=sdkpreparse" className="loginBtn loginBtn--facebook">Share on Facebook</a></div>
        </section>
        </div>
        )
    }
}
export default ListComponent