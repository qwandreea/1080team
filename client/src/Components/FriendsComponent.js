import React, { Component } from 'react'
import $ from 'jquery'
class FriendsComponent extends Component
{
   constructor()
   {
       super()
   }

   componentDidMount()
   {

    function getFriends(done){
        $.get('friends/myfriends', function(data){
          done(data)
        })
      }

      $(function(e){
        let friendList=$("#my-friends")
       getFriends(function(friends){            
            friendList.empty()
            for(var friend of friends){
                  friendList.append(createCard(friend))
            }
         })
      })


      function createCard(friend){
        return (`
            <div class="col-4 card mx-2 p-4">
            <h4 className="friend-name">${friend.nume}</h4>
            <div className="friend-tag">${friend.tag}</div>
            <div class="row">
                <div className="col m-3 p-3">
                </div>
            </div>
        </div>
        `)
      }
    }

   render(){
       return(
       <div>

<div className="container">
            <div className="row"></div>
           <h2>myfriends</h2>
           <div className="col"></div>

       <div className="row" id="my-friends">

           <div className="col-4 card mx-2 p-4">
               <h4 className="friend-name">Name</h4>
               <div className="friend-tag">Tag</div>
               <div className="row">
                   <div className="col m-3 p-3">
                   </div>
               </div>
           </div>
        

           <div className="col-4 card mx-2 p-4">
               <h4 className="friend-name">Name</h4>
               <div className="friend-tag">Tag</div>
           </div>

           <div className="col-4 card mx-2 p-4">
               <h4 className="friend-name">Name</h4>
               <div className="friend-tag">Tag</div>
           </div>
       </div>
        </div>

       </div>
          
       )
   }

}
export default FriendsComponent