import React, { Component } from 'react'
import './Home.css'

const photo=require('../img/loginFridge.png')

class HomeComponent extends Component {
  render() {
    return (
      // <div className="container">
      //   <div className="jumbotron mt-5">
      //     <div className="col-sm-8 mx-auto">
      //       <h1 className="text-center">WELCOME</h1>
      //     </div>
      //   </div>
      // </div>
      <div className="all">
          <div className="back-img">
            <div className="container">
               <div className="div-container">
                 <div className="div-text">
                     <h1 className="home-text">WELCOME TO YOUR FRIDGE</h1>
                   </div>
                 </div>
                </div>
              </div>
              <div className="fridge">
                <img src={photo} className="img-fridge" width="450px" height="380px" position="center"/>
              </div>
        </div>
    )
  }
}

export default HomeComponent