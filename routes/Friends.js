const express = require('express')
const friends = express.Router()
const cors = require('cors')

const Friend=require('../utils/Friend')
friends.use(cors())

friends.get('/myfriends',(req,res)=>{
    Friend.findAll().then((friend)=>{
        res.status(200).send(friend)
    })
    .catch((err)=>{
        res.status(500).send({
            err:"Not find"
        })
    })
})

friends.post('/myfriends',(req,res)=>{
        Friend.create({
            nume:req.body.nume,
            tag:req.body.tag
        })
     .then((friend)=>{
         res.status(201).send(friend)
     }).catch((error)=>{
         res.status(501).send({
             error:"Error while adding ..."
         })
     })
})



friends.put('/myfriends/:id',async(req,res)=>{
    try
    {
        let friends=await Friend.findByPk(req.params.id)
        if(friends){
            await friends.update(req.body)
            res.status(202).json({message:'updated'})
        }
        else
        {
            res.status(404).json({message:'not found'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
    
})

friends.delete('/myfriends/:id',async(req,res)=>{
    try
    {
        let friends=await Friend.findByPk(req.params.id)
        if(friends){
            await friends.destroy(req.body)
            res.status(202).json({message:'deleted'})
        }
        else
        {
            res.status(404).json({message:'not found'})
        }
    }
    catch(e)
    {
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
})


friends.put('/myfriends/:id',async(req,res)=>{
    try
    {
        let friends=await Friend.findByPk(req.params.id)
        if(friends){
            await friends.update(req.body)
            res.status(202).json({message:'updated'})
        }
        else
        {
            res.status(404).json({message:'not found'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
    
})


friends.put('/myfriends/:id/tag',async(req,res)=>{
    try
    {
        let friends=await Friend.findByPk(req.params.id)
        if(friends){
            await friends.update(req.body)
            res.status(202).json({message:'updated'})
        }
        else
        {
            res.status(404).json({message:'not found'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
    
})




module.exports=friends