const express = require('express')
const products = express.Router()
const cors = require('cors')



const Product=require('../utils/Product')
products.use(cors())

products.get('/products',(req,res)=>{
    //all products
    Product.findAll().then((product)=>{
        res.status(200).send(product)
    })
    .catch((err)=>{
        res.status(500).send({
            err:"Not find"
        })
    })
})

products.post('/products',(req,res)=>{
    if (isNaN(req.body.cantity)){
        res.status(403).send({
            error: "Cantity is not a number"
        })
    }
    else
    {
        Product.create({
            name:req.body.name,
            category:req.body.category,
            valability:Date.parse(req.body.valability),
            cantity:parseFloat(req.body.cantity),
            unit:req.body.unit,
            note:req.body.note
        })
     .then((product)=>{
         res.status(201).send(product)
     }).catch((error)=>{
         res.status(501).send({
             error:"Error while adding product.."
         })
     })
    }
   
})

// products.get('/products/:id',async(req,res)=>{
//     try{
//         let product= await Product.findByPk(req.params.id)
//         if(product){
//             res.status(200).json(product)
//         }
//         else
//         {
//             res.status(404).json({message:"not found"})
//         }
//     }
//     catch(e)
//     {
//         console.warn(e)
//         res.status(500).json({message:'server error'})
//     }
// })

products.put('/products/:id',async(req,res)=>{
    try
    {
        let product=await Product.findByPk(req.params.id)
        if(product){
            await product.update(req.body)
            res.status(202).json({message:'updated'})
        }
        else
        {
            res.status(404).json({message:'not found'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
    
})

products.delete('/products/:id',async(req,res)=>{
    try
    {
        let product=await Product.findByPk(req.params.id)
        if(product){
            await product.destroy(req.body)
            res.status(202).json({message:'deleted'})
        }
        else
        {
            res.status(404).json({message:'not found'})
        }
    }
    catch(e)
    {
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
})

products.get('/products/:category',async(req,res)=>{
    try{
        let product= await Product.findAll({
            where: {
               category:req.params.category
              }
		});
        if(product){
            res.status(200).json(product)
        }
        else
        {
            res.status(404).json({message:"not found"})
        }
    }
    catch(e)
    {
        console.warn(e)
        res.status(500).json({message:'server error'})
    }
})

module.exports=products