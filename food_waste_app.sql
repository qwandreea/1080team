-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: ian. 13, 2020 la 11:33 PM
-- Versiune server: 10.4.10-MariaDB
-- Versiune PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `food_waste_app`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `friends`
--

CREATE TABLE `friends` (
  `id` int(11) NOT NULL,
  `nume` text NOT NULL,
  `tag` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `friends`
--

INSERT INTO `friends` (`id`, `nume`, `tag`) VALUES
(1, 'Ionel', 'vegetarian'),
(2, 'Remus', 'mixt'),
(3, 'Cristina', 'carnivor');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `products`
--

CREATE TABLE `products` (
  `idp` int(11) NOT NULL,
  `name` text NOT NULL,
  `category` text NOT NULL,
  `valability` date NOT NULL,
  `cantity` float NOT NULL,
  `unit` text NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `products`
--

INSERT INTO `products` (`idp`, `name`, `category`, `valability`, `cantity`, `unit`, `note`) VALUES
(1, 'Mere', 'Fruits', '2020-01-16', 2, 'Kg', 'Verzi'),
(2, 'Kiwi', 'Fruits', '2020-01-15', 300, 'Grams', '-'),
(3, 'Oua', 'Dairy Products', '2020-01-17', 5, 'Pieces', 'Bio'),
(4, 'Carne de pui', 'Meat', '2020-01-16', 1, 'Kg', 'Comert'),
(5, 'Lapte ', 'Dairy Products', '2020-01-17', 1, 'Kg', 'De vaca'),
(6, 'Tort ', 'Sweets', '2020-01-18', 1, 'Kg', 'De ciocolata, de casa');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nume` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `users`
--

INSERT INTO `users` (`id`, `nume`, `email`, `password`) VALUES
(3, 'Andreea', 'test@mail.com', '$2a$10$ZnQ4eKqWeeckTNh7UCekyei13YQWUQO/CeeHri4chutLhGWebUYU.'),
(4, 'Maria', 'test12@mail.com', '$2a$10$j9/RjUUMi2uZwgslqtRvtOq0iCsS3tflmT5fVbGVVDCyQ/QOOaMcC');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`);

--
-- Indexuri pentru tabele `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idp`);

--
-- Indexuri pentru tabele `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pentru tabele `products`
--
ALTER TABLE `products`
  MODIFY `idp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pentru tabele `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
