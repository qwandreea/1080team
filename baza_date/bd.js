//Setare sequelize
const Sequelize=require("sequelize")
const bd={}
const sequelize=new Sequelize("food_waste_app","root","",{
    host:'localhost',
    dialect:'mysql',
    operatorAliases:false,

    //pool de conexiuni la initializare
    pool:{
        max:5,
        min:0,
        acquire:30000,
        idle:10000
    }
})

bd.sequelize=sequelize
bd.Sequelize=Sequelize
bd.sequelize.sync().then(()=>console.log("SYNC DATABASE")).catch((err)=>console.log("error"))
module.exports=bd
