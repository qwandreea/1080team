const Sequelize=require("sequelize")
const bd=require("../baza_date/bd")
module.exports=bd.sequelize.define(
    'friend',
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true
        },
        nume:{
            type:Sequelize.STRING
        },
        tag:{
            type:Sequelize.STRING
        }
    },
    {
        //dezactivare
        timestamps:false
    }
)