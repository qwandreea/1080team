const Sequelize=require("sequelize")
const bd=require("../baza_date/bd")

module.exports=bd.sequelize.define(
    'user',
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true
        },
        nume:{
            type:Sequelize.STRING
        },
        email:{
            type:Sequelize.STRING
        },
        password:{
            type:Sequelize.STRING
        }
    },
    {
        //dezactivare
        timestamps:false
    }
)
